<?php
$context = Timber::get_context();
$context['term'] = new TimberTerm(get_queried_object_id());
Timber::render('category.twig', $context);