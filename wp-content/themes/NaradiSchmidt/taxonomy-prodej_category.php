<?php
$context = Timber::get_context();
$context['term'] = new TimberTerm(get_queried_object_id());

global $paged;

if (!isset($paged) || !$paged){
    $paged = 1;
}
$args = array(
    'post_type' => 'prodej',
    'posts_per_page' => 16,
    'paged' => $paged,
    'tax_query' => array(
        array(
            'taxonomy' => 'prodej_category',
            'field' => 'term_id', 
            'terms' => $context['term']->term_id 
        )
    ),
    'orderby' => array(
        'date' => 'ASC'
    ),
);
$context['prodej'] = new Timber\PostQuery($args);

Timber::render('taxonomy/prodej_category.twig', $context);