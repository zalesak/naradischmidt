<?php
if (!class_exists('Timber')) {
    add_action('admin_notices', function () {
        echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url(admin_url('plugins.php#timber')) . '">' . esc_url(admin_url('plugins.php')) . '</a></p></div>';
    });

    add_filter('template_include', function ($template) {
        return get_stylesheet_directory() . '/static/no-timber.html';
    });

    return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array('templates', 'views');

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;

/**
 * Autoloader for libs
 */
spl_autoload_register(function ($class_name) {
    $filePath = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'libs' . DIRECTORY_SEPARATOR;
    $file = $filePath . $class_name . '.php';

    // only include if file exists, otherwise we might enter some conflicts with other pieces of code which are also using the spl_autoload_register function
    if (file_exists($file)) {
        include_once $file;
    }
});


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class Site extends Timber\Site
{
    /** Add timber support. */
    public function __construct()
    {
        $this->init_libs();
        $this->register_theme_settings();

        add_image_size('gallery_thumb', 500, 500, true);

        add_action('after_setup_theme', array($this, 'theme_supports'));
        add_filter('timber_context', array($this, 'add_to_context'));
        add_filter('get_twig', array($this, 'add_to_twig'));
        add_action('init', array($this, 'register_post_types'));
        add_action('init', array($this, 'register_taxonomies'));
        add_action('wp_enqueue_scripts', array($this, 'load_scripts'));
        add_action('wp_enqueue_scripts', array($this, 'load_styles'));
        parent::__construct();
    }

    /**
     * Init libs from directory
     */
    function init_libs()
    {
        new Tweaks();
        new DisableComments();
        new Branding();
    }

    /** This is where you can register custom post types. */
    public function register_post_types()
    {

        register_post_type('prodej', array(
            'hierarchical' => false,
            'public' => true,
            'label' => 'Prodej',
            'supports' => array('title', 'editor', 'page-attributes'),
            'rewrite' => array('slug' => 'prodej-detail', 'with_front' => false),
            'has_archive' => false,
        ));

        register_post_type('pujcovna', array(
            'hierarchical' => false,
            'public' => true,
            'label' => 'Půjčovna',
            'supports' => array('title', 'editor', 'page-attributes'),
            'rewrite' => array('slug' => 'pujcovna-detail', 'with_front' => false),
            'has_archive' => false,
        ));

    }

    /** This is where you can register custom taxonomies. */
    public function register_taxonomies()
    {
        register_taxonomy('prodej_category', 'prodej', array(
            'label' => 'Kategorie',
            'rewrite' => array('slug' => 'prodej-kategorie'),
            'hierarchical' => true,
        ));

        register_taxonomy('pujcovna_category', 'pujcovna', array(
            'label' => 'Kategorie',
            'rewrite' => array('slug' => 'pujcovna-kategorie'),
            'hierarchical' => true,
        ));
    }

    /** This is where you add some context
     *
     * @param string $context context['this'] Being the Twig's {{ this }}.
     * @return string
     */
    public function add_to_context($context)
    {
        $context['bestseller_pujcovna'] = get_field('bestseller_pujcovna', 'option');
        $context['bestseller_prodej'] = get_field('bestseller_prodej', 'option');
        $context['options'] = get_fields('option');
        $context['prodejCategories'] = Timber::get_terms('prodej_category');
        $context['pujcovnaCategories'] = Timber::get_terms('pujcovna_category');
        $context['menu'] = new Timber\Menu('Hlavní menu');
        $context['site'] = $this;
        return $context;
    }

    public function theme_supports()
    {
        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support(
            'html5', array(
                'comment-form',
                'comment-list',
                'gallery',
                'caption',
            )
        );

        /*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
        add_theme_support(
            'post-formats', array(
                'aside',
                'image',
                'video',
                'quote',
                'link',
                'gallery',
                'audio',
            )
        );

        add_theme_support('menus');
    }

    /** This is where you can add your own functions to twig.
     *
     * @param string $twig get extension.
     * @return string
     */
    public function add_to_twig($twig)
    {
        return $twig;
    }

    function register_theme_settings()
    {
        if (function_exists('acf_add_options_page')) {

            acf_add_options_page(array(
                'page_title' => 'Nastavení šablony',
                'menu_title' => 'Nastavení šablony',
                'menu_slug' => 'nastaveni-sablony',
                'capability' => 'edit_posts',
                'redirect' => false,
            ));

        }
    }

    public function load_scripts()
    {
        wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/vendor/bootstrap-4.2.1/js/bootstrap.bundle.min.js', array('jquery'), '', true);
        wp_enqueue_script('number-js', get_template_directory_uri() . '/assets/js/number.js', array('jquery'), '', true);
        wp_enqueue_script('main-js', get_template_directory_uri() . '/assets/js/main.js', array('jquery'), '', true);
        wp_enqueue_script('header-js', get_template_directory_uri() . '/assets/js/header.js', array('jquery'), '', true);
        wp_enqueue_script('owl-carousel', get_template_directory_uri() . '/assets/vendor/owl-carousel-2.3.4/owl.carousel.min.js', array('jquery'), '', true);
        wp_enqueue_script('nouislider', get_template_directory_uri() . '/assets/vendor/nouislider-12.1.0/nouislider.min.js', array('jquery'), '', true);
        wp_enqueue_script('photoswipe', get_template_directory_uri() . '/assets/vendor/photoswipe-4.1.3/photoswipe.min.js', array('jquery'), '', true);
        wp_enqueue_script('photoswipe-ui', get_template_directory_uri() . '/assets/vendor/photoswipe-4.1.3/photoswipe-ui-default.min.js', array('jquery'), '', true);
        wp_enqueue_script('select2', get_template_directory_uri() . '/assets/vendor/select2-4.0.10/js/select2.min.js', array('jquery'), '', true);
        wp_enqueue_script('svg4everybody', get_template_directory_uri() . '/assets/vendor/svg4everybody-2.1.9/svg4everybody.min.js', array('jquery'), '', true);
        wp_enqueue_script('overwrite-js', get_template_directory_uri() . '/assets/naradi-assets/js/main.js', array('jquery'), '', true);
    }

    public function load_styles()
    {
        wp_enqueue_style('bootstrap-css', get_template_directory_uri() . '/assets/vendor/bootstrap-4.2.1/css/bootstrap.min.css');
        wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/vendor/fontawesome-5.6.1/css/all.min.css');
        wp_enqueue_style('owl-carousel-2', get_template_directory_uri() . '/assets/vendor/owl-carousel-2.3.4/assets/owl.carousel.min.css');
        wp_enqueue_style('photoswipe-4', get_template_directory_uri() . '/assets/vendor/photoswipe-4.1.3/photoswipe.css');
        wp_enqueue_style('photoswipe-ui-4', get_template_directory_uri() . '/assets/vendor/photoswipe-4.1.3/default-skin/default-skin.css');
        wp_enqueue_style('select2-css', get_template_directory_uri() . '/assets/vendor/select2-4.0.10/css/select2.min.css');
        wp_enqueue_style('font-stroyka', get_template_directory_uri() . '/assets/fonts/stroyka/stroyka.css');
        wp_enqueue_style('roboto-font', 'https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i');
        wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css');
        wp_enqueue_style('overwrite-styles', get_template_directory_uri() . '/assets/naradi-assets/styles.css');
        wp_enqueue_style('daniel-styles', get_template_directory_uri() . '/assets/naradi-assets/daniel.css');
    }

}

new Site();
