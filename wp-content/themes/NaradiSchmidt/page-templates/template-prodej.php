<?php
/**
 * Template Name: Prodej
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

global $paged;

if (!isset($paged) || !$paged){
    $paged = 1;
}
$args = array(
    'post_type' => 'prodej',
    'posts_per_page' => 16,
    'paged' => $paged,
    'orderby' => array(
        'date' => 'DESC'
    ),
);
$context['prodej'] = new Timber\PostQuery($args);

Timber::render('prodej.twig', $context);