<?php
/**
 * Template Name: Půjčovna
 */

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;

global $paged;

if (!isset($paged) || !$paged){
    $paged = 1;
}
$args = array(
    'post_type' => 'pujcovna',
    'posts_per_page' => 16,
    'paged' => $paged,
    'orderby' => array(
        'date' => 'DESC'
    ),
);
$context['pujcovna'] = new Timber\PostQuery($args);

Timber::render('pujcovna.twig', $context);