<?php

/**
 * Class Branding
 * Add custom branding to admin
 */
class Branding
{

    function __construct()
    {
        add_action('login_headertext', array($this, 'custom_link_title'));
        add_action('login_head', array($this, 'custom_logo'));
        add_action('wp_dashboard_setup', array($this, 'remove_dashboard_widgets'));
        add_filter('login_headerurl', array($this, 'the_url'));
        add_action('load-customize.php', array($this, 'customize'));
        add_action('admin_menu', array($this, 'remove_submenus'));
        add_action('wp_before_admin_bar_render', array($this, 'remove_admin_bar_links'));
        add_action('admin_menu', array($this, 'remove_items_from_menu'));

        new DisableComments();

        define('DISALLOW_FILE_EDIT', true);
    }

    /**
     * Change URL for login screen
     * @return string
     */
    function the_url()
    {
        return get_bloginfo('url');
    }

    /**
     * Remove unnecessary widgets from admin dashboard
     */
    function remove_dashboard_widgets()
    {
        remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // Right Now
        remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // Recent Comments
        remove_meta_box('dashboard_activity', 'dashboard', 'normal');
        remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // Plugins
        remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
        remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
        remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog
        remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
        remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal');   // SEO
    }

    /**
     * Customize callback info
     */
    function customize()
    {
        // Disallow acces to an empty editor.
        wp_die(sprintf(__('No WordPress Theme Customizer support - If needed check your functions.php')) . sprintf('<br /><a href="javascript:history.go(-1);">Go back</a>'));
    }

    /**
     * Remove 'Customize' from Admin menu.
     */
    function remove_submenus()
    {
        global $submenu;
        // Appearance Menu
        unset($submenu['themes.php'][6]); // Customize
    }

    /**
     * Remove 'Customize' from the Toolbar -front-end.
     */
    function remove_admin_bar_links()
    {
        global $wp_admin_bar;
        $wp_admin_bar->remove_menu('customize');
    }

    function custom_link_title()
    {
        return get_bloginfo('title');
    }

    function custom_logo()
    {
        echo "<style> body.login #login h1 a { background: url('" . get_template_directory_uri() . "/assets/images/logo-new.png') no-repeat; width: 150px; height: 67px;margin: 50px auto;} body.login{background-color:#fff;) !important;}
.login #nav a, .login #backtoblog a {color:#596571 !important; }
.login #nav, .login #backtoblog { text-shadow: 0 0 0 #000; }
</style>";
    }

    /**
     * Remove items from menu for users
     */
    function remove_items_from_menu()
    {
        remove_menu_page('edit.php');

        $user = wp_get_current_user();
        if ($user->ID > 3) {
            remove_menu_page('options-general.php');
            remove_menu_page('plugins.php');
            remove_menu_page('tools.php');
            remove_menu_page('edit.php?post_type=acf-field-group');
            remove_submenu_page('index.php', 'update-core.php');
            remove_submenu_page('themes.php', 'themes.php');
        }
    }

}