<?php

/**
 * Class Tweaks
 * General tweaks for site
 */
class Tweaks
{
    function __construct()
    {
        add_action('acf/input/admin_head', array($this, 'my_acf_admin_head'));
        add_filter('wp_mail_content_type', array($this, 'email_content_type'));
    }

    /**
     * Assign main editor to custom field so it appears in "Content" custom field
     */
    function my_acf_admin_head()
    {

        ?>
        <script type="text/javascript">
            (function ($) {

                $(document).ready(function () {

                    $('.acf-field-5c1aaba118542 .acf-input').append($('#postdivrich'));

                });

            })(jQuery);
        </script>
        <style type="text/css">
            .acf-field #wp-content-editor-tools {
                background: transparent;
                padding-top: 0;
            }
        </style>
        <?php

    }

    /**
     * Allow to send HTML emails
     * @return string
     */
    function email_content_type()
    {
        return "text/html";
    }
}